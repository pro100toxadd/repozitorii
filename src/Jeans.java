/**
 * Created by Тоша on 30.05.2017.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Jeans {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        double money; // количество денег в кошельке
        double price; // цена на джинсы
        double discount; // скидка в процентах
        while(true) {
            System.out.print("Введите количество денег у покупателя, руб.: ");
            money = Double.parseDouble(reader.readLine());
            if (money>=0) break;
            else System.out.println("У вас не может быть денег меньше 0");
        }

        do {
            System.out.print("Введите цену на джинсы, руб.: ");
            price = Double.parseDouble(reader.readLine());
            if (price <= 0) System.out.println("Цена на джинсы должна быть больше 0");
        } while (price <= 0);

        do {
            System.out.print("Введите скидку, % (число): ");
            discount = Double.parseDouble(reader.readLine());
            if (discount >= 100 | discount < 0) System.out.println("Скидка должна быть больше 0 и меньше 100");
        } while (discount >= 100 | discount < 0);
        double sumJeans =price - (price*discount/100);
        System.out.println("Джинсы с учетом скидки стоят " + sumJeans + " рублей.");
        System.out.println("У вас " + money + " рублей.");

        if (money>=sumJeans) System.out.println("Вам хватет денег на джинсы");
        else System.out.println("Вам не хватет на джинсы");
    }
}